import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:philile_hlalukana_module4/screens/dashboard.dart';
import 'package:philile_hlalukana_module4/screens/login.dart';
import 'package:philile_hlalukana_module4/screens/register.dart';
import 'package:philile_hlalukana_module4/screens/user_profile_edit.dart';
import 'package:philile_hlalukana_module4/screens/welcome.dart';


main() {
  runApp(const MyQrAppHome());
}

class MyQrAppHome extends StatelessWidget {
  const MyQrAppHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: const SplashScreen(),

      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        Welcome.routeName: (context) => const Welcome(),
        SignupScreen.routeName: (context) => SignupScreen(),
        LoginScreen.routeName: (context) => LoginScreen(),
        DashboardMain.routeName: (context) => const DashboardMain(),
        ProfilePage.routeName: (context) => const ProfilePage(),
        EditProfilePage.routeName: (context) => const EditProfilePage(),
      },
    );// define it once at root level.
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 8), () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const Welcome(),
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return
      Center(
        child: Row(

          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              width: 300.0,
              height: 300.0,
              child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0Czw7FRmiGiIqKr03FXwBU0ihxmGUpVc-Wat2-ENju7LKKU5JbshaeHRDtLmzOg9JiuY&usqp=CAU"),

            ),


            const SizedBox(width: 20.0, height: 100.0),
            const Text(
              'QR',
              style: TextStyle(fontSize: 40.0,fontFamily: 'Horizon', color:Colors.blue),

            ),
            const SizedBox(width: 20.0, height: 100.0),
            DefaultTextStyle(
              style: const TextStyle(
                fontSize: 40.0,
                fontFamily: 'Horizon',
              ),
              child: Center(
                child: AnimatedTextKit(
                  animatedTexts: [
                    RotateAnimatedText('GIVES'),
                    RotateAnimatedText('YOU'),
                    RotateAnimatedText('ACCESS'),

                  ],
                ),
              ),
            ),
          ],
        ),

      );

  }
}


